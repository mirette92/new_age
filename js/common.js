app.filter("trustUrl", ['$sce', function ($sce) {
    return function (recordingUrl) {
        return $sce.trustAsResourceUrl(recordingUrl);
    };
}]);


app.service('SrvUser',[ '$http','toaster',function($http,toaster) { 
var service = {
       Login: function(obj){
            return $http.post('https://newagefitness.ae/api/login',obj).catch(function(response){
               if(response.status != 200){
                // $scope.loginSpinner=false;
                toaster.pop({
                    type: 'error',
                    title: 'login',
                    body: 'failed to login',
                    timeout: 3000
                });
               }
            });
        },
		Register:function(obj){
			return $http.post('https://newagefitness.ae/api/register',obj).catch(function(response){
				if(response.status != 200){
                    // $scope.registerSpinner=false;
                    toaster.pop({
                        type: 'warning',
                        title: 'Register',
                        body: 'user aready exist',
                        timeout: 3000
                    });
                   }
			});
        },
        Verify:function(obj){
			return $http.post('https://newagefitness.ae/api/verify',obj).catch(function(response){
				if(response.status != 200){
                    // $scope.verifySpinner=false;
                    toaster.pop({
                        type: 'error',
                        title: 'failed',
                        body: 'failed to verify invalid code',
                        timeout: 3000
                    });
                   }
			});
        },
        Reset:function(obj){
            return $http.post('https://newagefitness.ae/api/requestresetpassword',obj).catch(function(response){
				if(response.status != 200){
                    // $scope.verifySpinner=false;
                    toaster.pop({
                        type: 'error',
                        title: 'failed',
                        body: 'failed to verify invalid code',
                        timeout: 3000
                    });
                   }
			});
        }
				
    }
    return service;
}]);

app.service('SrvVideo',[ '$http',function($http) { 
    var service = {
        getSchedule: function(){
                return $http.get('https://newagefitness.ae/api/exercises').success(function(response){
                  
                });
            }
           
                    
        }
        return service;
    }]);


    app.service('SrvCountry',[ '$http',function($http) { 
        var service = {
            list: function(){
                    return $http.get('https://newagefitness.ae/api/getCountryList').success(function(response){
                      
                    });
                }
               
                        
            }
            return service;
        }]);    

    app.service('SrvClasses',[ '$http',function($http) { 
        var service = {
            listFeature: function(){
                    return $http.get('https://newagefitness.ae/api/exercises/featured').success(function(response){
                        
                    });
                },
            listPast: function(){
                return $http.get('https://newagefitness.ae/api/exercises/past').success(function(response){
                    
                });
            },
            listToday: function(){
                return $http.get('https://newagefitness.ae/api/exercises/today').success(function(response){
                    
                });
            }      
                
                        
            }
            return service;
        }]); 
