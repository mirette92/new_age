var app = angular.module('myApp', ['toaster', 'ngAnimate']);

app.controller('myCtrl', function($scope,SrvUser,toaster,$timeout,SrvCountry){

     $scope.objLogin = {email:'',password:''}; 
    $scope.objRegister = {email:'',firstName:'',lastName:'',password:'',birthdate:'',gender:'',mobile:'',strCountry:"0",country:"0"};
    $scope.loginSpinner = false;
    $scope.registerSpinner = false;
    $scope.verifySpinner = false;
    $scope.resetSpinner = false;
    $scope.objVerify = {'code':'','email':''}
    $scope.toaster = true;
    $scope.ferror = false;
    $scope.message = '';
    $scope.MDetails = "";
    $scope.className = "";
    $scope.arrCountry = [];
    $scope.objReset = {email:'',Password:''};
    $scope.lang = "ar";
    // $scope.error1 = false;

    $scope.init = function(){
        // $scope.lang = if(localStorage.getItem('langW')=='en')? $scope.lang = 'en': $scope.lang = 'ar'
        $scope.listCountry();
    }
    // $scope.init();
    // $scope.pop = function() {
        
    //   };
    $scope.listCountry = function(){
        SrvCountry.list().then(function(response){
            $scope.arrCountry = response.data;
        })
    }
    $scope.openModal = function(x){
        if(x==1){
            $scope.MDetails = "Perfect shape as the name explains – shapes and sculpts the whole body using body weight, resistant bands, and dumbbells. Perfect for the ladies who want to have that perfect body.";
            $scope.className = "My Perfect Shape";
        }if(x==2){
            $scope.className = "FuncFit";
            $scope.MDetails = "FuncFit – is a full body workout focusing on natural movements that enhance the activities for our daily life.";
        }if(x==3){
            $scope.className = "HIIT/Core";
            $scope.MDetails = "HIIT is a high intensity class that shapes and tone the body with use of dumbbells, plates, body bars and bands can be used to perform the various exercises.";
        }if(x==4){
            $scope.className = "Strong by Zumba";
            $scope.MDetails = "Using a form of high-intensity interval training, Strong by Zumba customizes the music to match each move of your routine. They promise maximum calorie loss and ensure that you not only finish the workout but exceed your expectations.";
        }if(x==5){
            $scope.className = "Zumba";
            $scope.MDetails = "ZUMBA is one of the most popular group exercise classes on the planet. It’s a Latin inspired, high energy dance party workout that takes you to the dance floor";
        }if(x==6){
            $scope.className = "Bollywood";
            $scope.MDetails = "Get the best dance cardio workout of your life with our fantastically popular Bollywood Workout Class. This powerful workout sets workout cycles to the latest upbeat songs from Bollywood Movies. All experience levels encouraged, beginners welcome to sweat and have FUN together.";
        }if(x==7){
            $scope.className = "Yoga";
            $scope.MDetails = "Yoga is a holistic approach into uniting the mind, body and soul. It calms your mind and boost your spirit and energy.";
        }
        if(x==8){
            $scope.className = "Pilates";
            $scope.MDetails = "Pilates helps to improves flexibility, builds strength & develops control & endurance in the entire body. It puts emphasis on alignment, breathing, developing a strong core, & improving coordination & balance.";
        }if(x==9){
            $scope.className = "Latin Fitness";
            $scope.MDetails = "All the Caribbean flavor and Latin dance on a perfect class to learn, burn calories, body shaping and styling posture. You will be able to know, learn, different and enjoy the Latin rhythms that are multifaceted countless number of classes music variations though dynamics classes with Latin instructor.";
        }if(x==10){
            $scope.className = "BoxFit";
            $scope.MDetails = "Boxfit is an exercise class based on the training concepts boxers use to keep fit. Classes can take a variety of formats but a typical one may involve shadow-boxing, hitting pads, kicking punch bags, press-ups, shuttle runs & sit ups.";
        }if(x==11){
            $scope.className = "Core/Stretch";
            $scope.MDetails = "Core and Stretch workout are designed to lengthen your muscles and tighten up your abdomen. This workout aims to tone and flatten your stomach, help you bending and reaching, improve your movement/range of motion and relieve stress. ";
        }if(x==12){
            $scope.className = "Dancex";
            $scope.MDetails = "Dancx workout is a cardio-intense, follow along with fitness variations using all type of music and rhythms. Inspiring dance fitness for everyone.";
        }if(x==13){
            $scope.className = "STRENGTH & CONDITIONING";
            $scope.MDetails = "Strength & Conditioning Program perfectly designed to improve Muscular Strength, Stamina and Body Conditioning. This Program is designed in such a way that Every Fitness Enthusiast can perform and get Strength & Conditioning Result.";
        }if(x==14){
            $scope.className = "JUST CORE";
            $scope.MDetails = "JUST CORE will help you to have Chiseled waist, improve core strength and helps to improve athletic performance. Everyone can perform it and get benefits from this amazing workout.";
        }if(x==15){
            $scope.className = "HIIT";
            $scope.MDetails = "HIIT is an intense performance oriented program which will challenge you in Fun Way to improve aerobic capacity and muscular endurance and helps to get in Shape and Burn Maximum Calories.";
        }if(x==16){
            $scope.className = "FIT FAMILY";
            $scope.MDetails = "Fit Family is a FUN based home workout program based on bodyweight exercises that improves activities of daily life, burn calories, improve movement quality and helps to fight stress by keeping the Family Together.  This is FUN based exercise Program suitable for all Family Members.";

        }
        $("#myDModal").modal()
    }
    $scope.login = function(){
        // window.location.href = "videos.html";return;
        // $scope.objLogin = $scope.objLogin;
       
        SrvUser.Login($scope.objLogin).then(function(response){
            if(response.data != null){
                $scope.loginSpinner=false;
                toaster.pop({
                    type: 'Success',
                    title: 'login',
                    body: 'logged it successfully',
                    timeout: 3000
                });
                window.location.href = "videos.html";
                // $timeout(function() {
                //     $location.path('videos.html');
                // }, 3000);
            }
        }).catch(function(e){
            $scope.loginSpinner=false;
        })
        
     }
    $scope.selectCountry = function(obj){
        obj = JSON.parse(obj)
        $scope.objRegister.country_code = obj.calling_code;
        $scope.objRegister.country = obj.country;
    }
    $scope.register = function(){
        var x = $scope.objRegister.strCountry.split("-");
        $scope.objRegister.country_code = x[0];
        $scope.objRegister.country = x[1];
        if($scope.objRegister.firstName == ''|| $scope.objRegister.lastName == '' || $scope.objRegister.gender == ''||$scope.objRegister.email == ''||$scope.objRegister.password == ''||$scope.objRegister.mobile == ''){
            $scope.ferror = true;
            $scope.message = 'All Inputs is required expect Age';
            return;
        }
        $scope.registerSpinner=true;
        SrvUser.Register($scope.objRegister).then(function(response){
            $scope.registerSpinner=false;
            if(response.data.email == undefined){
                toaster.pop({
                    type: 'error',
                    title: 'Register',
                    body: 'user is register before',
                    timeout: 3000
                }); 
                return;
            }
            if(response.status == 200){
                toaster.pop({
                    type: 'Success',
                    title: 'Register',
                    body: 'you register successfully get verify code from mail',
                    timeout: 3000
                }); 
                $("#myModal").modal()
                localStorage.setItem("userEmail", response.data.email);
            }
        })
        
    }
    $scope.langC = function(){
        if($scope.lang == 'en'){
            window.location.href = "index.html";
        }if($scope.lang=='ar'){
            
            
            
        }
        else{
            
        }
    }
    $scope.verify = function(){
        $scope.verifySpinner = true;
        $scope.objVerify = {'code':$scope.objVerify.code,'email':localStorage.getItem("userEmail")} 
        SrvUser.Verify($scope.objVerify).then(function(response){
            
            if(response.status == 200){
                $scope.verifySpinner = false;
                // toaster.pop({
                //     type: 'Success',
                //     title: 'Verify',
                //     body: 'the entered code is correct',
                //     timeout: 3000
                // });
                window.location.href = "videos.html";
                // $timeout(function() {
                //     $location.path('videos.html');
                // }, 3000);
            }
        })
    }
    
	$scope.showHide = function(){
        if(document.getElementById("mySidenav").style.display == "block"){
        document.getElementById("mySidenav").style.display = "none";
        }else{
        document.getElementById("mySidenav").style.display = "block";
        }
    }
    $scope.resetPassword= function(){
        
       
        $scope.resetSpinner = true;
        console.log($scope.objReset);
        SrvUser.Reset($scope.objReset).then(function(response){
            if(response.status =='200'){
                
                $scope.resetSpinner = false;
                toaster.pop({
                    type: 'Success',
                    title: 'success',
                    body: 'check you mailfor varifcation',
                    timeout: 3000
                });
                $("#LogD").modal('hide');
            }
        })
    }
    $scope.openResetD = function(){
        $("#LogD").modal()
    }
	$scope.init();
})

