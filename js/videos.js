var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope,SrvVideo,SrvClasses){

    $scope.arrGroupExercise = [];
    $scope.arrFitness = [];
    $scope.arrFeatureFitness = [];
    $scope.arrFeatureGroup = [];
    $scope.arrPastFitness = [];
    $scope.arrPastGroup = [];
    $scope.arrTodayGroup = [];
    $scope.arrTodayFitness = [];
    $scope.VName = '';
    $scope.try = '';
    $scope.value = 3;
    $scope.value1 = 6;
    $scope.valueMissedClass=10;
    $scope.valueFMissedClass=13
    $scope.lang = "en";
    $scope.sunday = {'day':'Sunday','time':["10:00","11:00"]}
    $scope.Monday = {'day':'Monday','time':["10:00","11:00","17:00"]}
    $scope.Tuesday = {'day':'Tuesday','time':["10:00","11:00","17:00"]}
    $scope.Wednesday = {'day':'Wednesday','time':["10:00","11:00","17:00"]}
    $scope.Thursday = {'day':'Thursday','time':["10:00","11:00","17:00"]}
    $scope.Friday = {'day':'Friday','time':["10:00","11:00","17:00","18:00","19:00"]}
    $scope.Saturday = {'day':'Saturday','time':["10:00","11:00","17:00"]}
    
    $scope.Schedule = [$scope.sunday,$scope.Monday,$scope.Tuesday,$scope.Wednesday,$scope.Thursday,$scope.Friday]                
    $scope.init = function(){
        $scope.PastClasses();
        $scope.FeatureClasses();
        $scope.TodayClasses();
        SrvVideo.getSchedule().then(function(response){
            angular.forEach(response.data, function(value, key) {
                if(value.category == "group_exercise"){
                    var videoId = $scope.getId(value.link);
                    value.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    // $scope.try = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    // $scope.try = '<iframe width="560" height="315" src="//www.youtube.com/embed/' 
                    // + videoId + '" frameborder="0" allowfullscreen></iframe>';
                    value.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";
                    var dateTime1 = value.date.split("T")
                    var date = dateTime1[0].split("-");
                    value.date = date[2] +"-"+ date[1] +"-"+ date[0]
                    if(value.time > "12:00"){
                        var t = value.time.split(":")
                        value.time = parseInt(value.time) - 12
                        value.time = value.time + ":"+ t[1]+" PM"
                    }else{
                        value.time = value.time + " AM"
                    }
                    $scope.arrGroupExercise.push(value);
                }else{
                    var videoId = $scope.getId(value.link);
                    value.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    var dateTime1 = value.date.split("T")
                    var date = dateTime1[0].split("-");
                    value.date = date[2] +"-"+ date[1] +"-"+ date[0]
                    if(value.time > "12:00"){
                        var t = value.time.split(":")
                        value.time = parseInt(value.time) - 12
                        value.time = value.time + ":"+ t[1]+" PM"
                    }else{
                        value.time = value.time + " AM"
                    }
                    value.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0"
                    $scope.arrFitness.push(value);
                }
              });
        })
    }
    $scope.PastClasses = function(){
        SrvClasses.listPast().then(function(response){
            angular.forEach(response.data, function(value, key) {
                if(value.category == "group_exercise"){
                    var videoId = $scope.getId(value.link);
                    value.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    // $scope.try = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    // $scope.try = '<iframe width="560" height="315" src="//www.youtube.com/embed/' 
                    // + videoId + '" frameborder="0" allowfullscreen></iframe>';
                    var dateTime1 = value.date.split("T")
                    var date = dateTime1[0].split("-");
                    value.date = date[2] +"-"+ date[1] +"-"+ date[0]
                    if(value.time > "12:00"){
                        var t = value.time.split(":")
                        value.time = parseInt(value.time) - 12
                        value.time = value.time + ":"+ t[1]+" PM"
                    }else{
                        value.time = value.time + " AM"
                    }
                    value.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";
                    $scope.arrPastGroup.push(value);
                }else{
                    var videoId = $scope.getId(value.link);
                    value.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    var dateTime1 = value.date.split("T")
                    var date = dateTime1[0].split("-");
                    value.date = date[2] +"-"+ date[1] +"-"+ date[0]
                    if(value.time > "12:00"){
                        var t = value.time.split(":")
                        value.time = parseInt(value.time) - 12
                        value.time = value.time + ":"+ t[1]+" PM"
                    }else{
                        value.time = value.time + " AM"
                    }
                    value.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0"
                    $scope.arrPastFitness.push(value);
                }
              });
        })
    }
    $scope.FeatureClasses = function(){
        SrvClasses.listFeature().then(function(response){
            angular.forEach(response.data, function(value, key) {
                if(value.category == "group_exercise"){
                    var videoId = $scope.getId(value.link);
                    value.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    // $scope.try = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    // $scope.try = '<iframe width="560" height="315" src="//www.youtube.com/embed/' 
                    // + videoId + '" frameborder="0" allowfullscreen></iframe>';
                    var dateTime1 = value.date.split("T")
                    var date = dateTime1[0].split("-");
                    value.date = date[2] +"-"+ date[1] +"-"+ date[0]
                    if(value.time > "12:00"){
                        var t = value.time.split(":")
                        value.time = parseInt(value.time) - 12
                        value.time = value.time + ":"+ t[1]+" PM"
                    }else{
                        value.time = value.time + " AM"
                    }
                    value.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";
                    $scope.arrFeatureGroup.push(value);
                }else{
                    var videoId = $scope.getId(value.link);
                    value.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    var dateTime1 = value.date.split("T")
                    var date = dateTime1[0].split("-");
                    value.date = date[2] +"-"+ date[1] +"-"+ date[0]
                    if(value.time > "12:00"){
                        var t = value.time.split(":")
                        value.time = parseInt(value.time) - 12
                        value.time = value.time + ":"+ t[1]+" PM"
                    }else{
                        value.time = value.time + " AM"
                    }
                    value.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0"
                    $scope.arrFeatureFitness.push(value);
                }
              });
        })
    }
    $scope.TodayClasses = function(){
        SrvClasses.listToday().then(function(response){
            angular.forEach(response.data, function(value, key) {
                if(value.category == "group_exercise"){
                    var videoId = $scope.getId(value.link);
                    value.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    // $scope.try = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    // $scope.try = '<iframe width="560" height="315" src="//www.youtube.com/embed/' 
                    // + videoId + '" frameborder="0" allowfullscreen></iframe>';
                    var dateTime1 = value.date.split("T")
                    var date = dateTime1[0].split("-");
                    value.date = date[2] +"-"+ date[1] +"-"+ date[0]
                    if(value.time > "12:00"){
                        var t = value.time.split(":")
                        value.time = parseInt(value.time) - 12
                        value.time = value.time + ":"+ t[1]+" PM"
                    }else{
                        value.time = value.time + " AM"
                    }
                    value.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";
                    $scope.arrTodayGroup.push(value);
                }else{
                    var videoId = $scope.getId(value.link);
                    value.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
                    var dateTime1 = value.date.split("T")
                    var date = dateTime1[0].split("-");
                    value.date = date[2] +"-"+ date[1] +"-"+ date[0]
                    if(value.time > "12:00"){
                        var t = value.time.split(":")
                        value.time = parseInt(value.time) - 12
                        value.time = value.time + ":"+ t[1]+" PM"
                    }else{
                        value.time = value.time + " AM"
                    }
                    value.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0"
                    $scope.arrTodayFitness.push(value);
                }
              });
        })
    }
    $scope.changeDiv = function(v){
        $scope.value = v;
        
    }
    $scope.video = function(obj){
        localStorage.setItem("embVideo", obj.emb);
        window.location.href = "class.html";
    }
    
    $scope.getId = function(url) {
        url = url.trim();
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
        const match = url.match(regExp);
    
        return (match && match[2].length === 11)
          ? match[2]
          : null;
    }
    $scope.openModal = function(x){
        if(x==1){
            $scope.MDetails = "Perfect shape as the name explains – shapes and sculpts the whole body using body weight, resistant bands, and dumbbells. Perfect for the ladies who want to have that perfect body.";
            $scope.className = "My Perfect Shape";
            // $("#src").attr("src","images/My_Perfect_Shape_Teaser.mp4");
            var video = document.getElementById('videoPlay');
            video.src = "images/My_Perfect_Shape_Teaser.mp4"
            $scope.VName = "images/My_Perfect_Shape_Teaser.mp4"
            video.play();
            // $("#src").val() = "images/My_Perfect_Shape_Teaser.mp4"
        }if(x==2){
            $scope.className = "FuncFit";
            $scope.MDetails = "FuncFit – is a full body workout focusing on natural movements that enhance the activities for our daily life.";
            var video = document.getElementById('videoPlay');
            video.src = "images/FuncFit_Teaser.mp4"
            $scope.VName = "FuncFit_Teaser.mp4"
            video.play();
        }if(x==3){
            $scope.className = "HIIT/Core";
            $scope.MDetails = "HIIT is a high intensity class that shapes and tone the body with use of dumbbells, plates, body bars and bands can be used to perform the various exercises.";
            $scope.VName = ""
        }if(x==4){
            $scope.className = "Strong by Zumba";
            $scope.MDetails = "Using a form of high-intensity interval training, Strong by Zumba customizes the music to match each move of your routine. They promise maximum calorie loss and ensure that you not only finish the workout but exceed your expectations.";
            $scope.VName = ""
        }if(x==5){
            $scope.className = "Zumba";
            $scope.MDetails = "ZUMBA is one of the most popular group exercise classes on the planet. It’s a Latin inspired, high energy dance party workout that takes you to the dance floor";
            var video = document.getElementById('videoPlay');
            video.src = "images/Zumba_Teaser.mp4"
            $scope.VName = "Zumba_Teaser.mp4"
            video.play();
        }if(x==6){
            $scope.className = "Bollywood";
            $scope.MDetails = "Get the best dance cardio workout of your life with our fantastically popular Bollywood Workout Class. This powerful workout sets workout cycles to the latest upbeat songs from Bollywood Movies. All experience levels encouraged, beginners welcome to sweat and have FUN together.";
            $scope.VName = ""
        }if(x==7){
            $scope.className = "Yoga";
            $scope.MDetails = "Yoga is a holistic approach into uniting the mind, body and soul. It calms your mind and boost your spirit and energy.";
            $scope.VName = ""
        }
        if(x==8){
            $scope.className = "Pilates";
            $scope.MDetails = "Pilates helps to improves flexibility, builds strength & develops control & endurance in the entire body. It puts emphasis on alignment, breathing, developing a strong core, & improving coordination & balance.";
            $scope.VName = ""
        }if(x==9){
            $scope.className = "Latin Fitness";
            $scope.MDetails = "All the Caribbean flavor and Latin dance on a perfect class to learn, burn calories, body shaping and styling posture. You will be able to know, learn, different and enjoy the Latin rhythms that are multifaceted countless number of classes music variations though dynamics classes with Latin instructor.";
            $scope.VName = ""
        }if(x==10){
            $scope.className = "BoxFit";
            $scope.MDetails = "Boxfit is an exercise class based on the training concepts boxers use to keep fit. Classes can take a variety of formats but a typical one may involve shadow-boxing, hitting pads, kicking punch bags, press-ups, shuttle runs & sit ups.";
            $scope.VName = ""
        }if(x==11){
            $scope.className = "Core/Stretch";
            $scope.MDetails = "Core and Stretch workout are designed to lengthen your muscles and tighten up your abdomen. This workout aims to tone and flatten your stomach, help you bending and reaching, improve your movement/range of motion and relieve stress. ";
            $scope.VName = ""
        }if(x==12){
            $scope.className = "Dancex";
            $scope.MDetails = "Dancx workout is a cardio-intense, follow along with fitness variations using all type of music and rhythms. Inspiring dance fitness for everyone.";
            $scope.VName = ""
        }if(x==13){
            $scope.className = "STRENGTH & CONDITIONING";
            $scope.MDetails = "Strength & Conditioning Program perfectly designed to improve Muscular Strength, Stamina and Body Conditioning. This Program is designed in such a way that Every Fitness Enthusiast can perform and get Strength & Conditioning Result.";
            $scope.VName = ""
        }if(x==14){
            $scope.className = "JUST CORE";
            $scope.MDetails = "JUST CORE will help you to have Chiseled waist, improve core strength and helps to improve athletic performance. Everyone can perform it and get benefits from this amazing workout.";
            $scope.VName = ""
        }if(x==15){
            $scope.className = "HIIT";
            $scope.MDetails = "HIIT is an intense performance oriented program which will challenge you in Fun Way to improve aerobic capacity and muscular endurance and helps to get in Shape and Burn Maximum Calories.";
            $scope.VName = ""
        }if(x==16){
            $scope.className = "FIT FAMILY";
            $scope.MDetails = "Fit Family is a FUN based home workout program based on bodyweight exercises that improves activities of daily life, burn calories, improve movement quality and helps to fight stress by keeping the Family Together.  This is FUN based exercise Program suitable for all Family Members.";
            $scope.VName = ""

        }
        $("#myDModal").modal()
    }
    $scope.langC = function(){
        if($scope.lang == 'en'){
            
        }if($scope.lang=='ar'){
            
            window.location.href = "videos-ar.html";
            
        }
        else{
            
        }
    }
     $scope.init();   
    
	
})

